<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pages?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// P
	'pages_description' => 'اين پلاگين اجازه‌ي ايجاد صفحه‌هاي مقاله‌هايي را مي‌دهد كه به هيچ سلسله مراتبي متكي نيستند. 
در عضو مي‌توانند به نام يك اسلكت مرتبط شوند. 
اين پلاگين اجازه‌ي ايجاد صفحه‌هايي مانند اطلاعات حقوقي، در باره‌ي ما، تماس با ما و غيره را خواهد داد.',
	'pages_slogan' => 'صفحه‌هاي بدون بخش',
];
