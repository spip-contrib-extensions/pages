<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pages?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// P
	'pages_description' => 'Este plugin permite criar páginas de matérias que não estejam vinculadas a nenhuma hierarquia específica.
Em contrapartida, elas podem ser associadas a um nome de template.
Isto permite, por exemplo, a criação de páginas de aviso legal, sobre o site, contato etc.',
	'pages_slogan' => 'Páginas sem seção',
];
