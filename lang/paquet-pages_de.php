<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pages?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// P
	'pages_description' => 'Dieses Plugin ermöglicht es, Artikel zu erstellen, die mit keiner bestimmen Hierarchie verbunden sind.
Sie können hingegen mit dem Namen eines Skeletts verknüpft werden.
Das ist besonders für Impressumsseiten etc. nützlich.',
	'pages_slogan' => 'Seiten ohne Rubrik',
];
