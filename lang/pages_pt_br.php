<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pages?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// A
	'aucune_page' => 'Nehuma página, ainda.',

	// B
	'bouton_generer_page_utile' => 'Criar a página <strong>@titre@ (<em>@page@</em>)</strong>',
	'bouton_generer_pages_utiles' => 'Criar <em>todas</em> as páginas úteis',

	// C
	'convertir_article' => 'Converter em matéria',
	'convertir_page' => 'Converter em página',
	'creer_page' => 'Criar uma nova página',

	// E
	'erreur_champ_page_doublon' => 'Este identificador já existe',
	'erreur_champ_page_format' => 'Apenas caracteres alfanuméricos em minúsculas ou "_"',
	'erreur_champ_page_taille' => 'Máximo de 255 caracteres',
	'explication_pages_utiles' => 'Os templates atuais do site podem usar as seguintes paginas.',

	// I
	'info_page_unique' => 'Uma página',
	'info_pages_uniques' => '@nb@ páginas',

	// L
	'label_champ_page' => 'Página:',

	// M
	'modifier_page' => 'Alterar a página:',

	// P
	'pages_uniques' => 'Páginas únicas',

	// T
	'titre_page' => 'Página',
	'titre_pages_utiles' => 'Páginas úteis',
	'toutes_les_pages' => 'Todas as páginas',
];
