<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pages?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// P
	'pages_description' => 'Ce plugin permet de créer des pages d’articles qui ne sont reliées à aucune hiérarchie particulière.
En revanche elles peuvent être associées à un nom de squelette.
Cela permet notamment de créer des pages de notice légale, d’à-propos, de contact, etc.',
	'pages_slogan' => 'Des pages sans rubrique',
];
