<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pages?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// P
	'pages_description' => 'Met deze plugin kun je bladzijdes met artikelen maken die niet in de hiërarchie van de site zijn opgenomen.
Ze kunnen worden gekoppeld aan de naam van een skelet.
Op deze manier kunnen bladzijdes zoals wettelijke vermeldingen, contactinformatie, disclaimers, enz. worden gemaakt.',
	'pages_slogan' => 'Bladzijdes zonder rubriek',
];
