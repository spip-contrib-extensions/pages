<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pages?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// P
	'pages_description' => 'Tento zásuvný modul vám umožňuje vytvárať stránky s článkami, ktoré nemajú žiadnu konkrétnu hierarchickú štruktúru.
Môžete ich však prepojiť s názvom šablóny.
To vám umožňuje vytvárať stránky s informáciami právneho charakteru, časové osi, zmluvy, a i.',
	'pages_slogan' => 'Stránky bez rubriky',
];
