<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pages?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// P
	'pages_description' => 'This plugin allows you to create pages of articles that are not linked to any particular hierarchy.
However they may be associated with a name template.
This allows the creation of pages of legal information, about, contact, etc..
',
	'pages_slogan' => 'Unlinked pages',
];
