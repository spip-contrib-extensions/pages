<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pages?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// A
	'aucune_page' => 'Momentálne tu nie je žiadna stránka.',

	// C
	'convertir_article' => 'Zmeniť na článok',
	'convertir_page' => 'Zmeniť na stránku',
	'creer_page' => 'Vytvoriť novú stránku',

	// E
	'erreur_champ_page_doublon' => 'Tento identifikátor už existuje',
	'erreur_champ_page_format' => 'Malé písmená alebo jeden "_"', # MODIF
	'erreur_champ_page_taille' => 'Maximum je 255 znakov', # MODIF

	// L
	'label_champ_page' => 'Stránka:',

	// M
	'modifier_page' => 'Upraviť stránku:',

	// P
	'pages_uniques' => 'Jedinečné stránky',

	// T
	'titre_page' => 'Stránka',
	'toutes_les_pages' => 'Všetky stránky',
];
